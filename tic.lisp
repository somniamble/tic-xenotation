(defparameter *prime-filename* "primes1000.txt")
(defparameter *primes*
  (let ((primes  (with-open-file (s *prime-filename*)
		   (loop for p = (read s nil)
			 while p collect p))))
    (make-array (length primes) :initial-contents primes)))


(defun nth-prime (n)
  "retrieves the nth (1-indexed) prime"
  (aref *primes* (1- n) ))

(defun implex (n)
  "raises a number's hyprime index by 1, or, n -> (n)"
  (nth-prime n))


(defun resolve (&rest args)
  "Resolves a 0X (tic-xenotation-zero) form into a natural number"
  ;; no arguments, so return multiplicative identity
  (cond ((null args) 1)
	;; otherwise, car/cdr recursion
	;; unwrap the first argument and resolve it, implex as we come back up
	(t (* (implex (apply #'resolve (car args)))
	      ;; apply resolve to the rest of the arguments, which keeps
	      ;; them at the same level of resolution/implexion
	      (apply #'resolve (cdr args))))))


;;; this function can be used to retrieve the hyprime index of a prime,
;;; or, alternatively, it can be used to determine if a number is prime or composite
(defun hyprime-index (n)
  "retrieves the hyprime index of a number n, or nil if it is composite"
  (labels ((stepper (index &aux (curr (aref *primes* (1- index))))
	     (cond ((= curr n) index)
		   ((> curr n) nil)
		   (t (stepper (1+ index))))))
    (stepper 1)))

(defun divided-p (x y)
  "determines if y divides x -- e.g. when x % y == 0"
  (zerop (mod x y)))

;;; it is an invariant of this function that (= (reduce #'* (prime-factors <n>)) <n>) where n is some number
;;; basically, if you get the combined product of the output, it should equal the input
;;; also, this function will break if you pass it a number which is larger than the largest value of *primes*
(defun prime-factors (n)
  "produces the prime-factors of n as a list (which tends to be sorted low to high)"
  (if (hyprime-index n) (list n) ;check if n is prime, and just return it
      ;; for each prime less than n
      ;; check if p divides n
      ;; when p divides n, collect just that value
      (let ((factor (loop for p across *primes*
			  when (divided-p n p)
			    return p)))
	;; build up a list of prime factors of n, starting with p,
	;; consing it on to the prime factors of (n / p)
	(cons factor (prime-factors (/ n factor))))))

(defun to-0x (n &aux (is-composite (not (hyprime-index n))))
  "turns a natural number n into a 0-tic-xenotation"
  (labels ((worker (number)
	     (let ((factors (prime-factors number)))
	       (cond ((= 2 number) ())
		     ((= (length factors) 1)
		      (list (worker (hyprime-index number))))
		     (t (cons 'composite (mapcar #'worker factors)))))))
    (if is-composite
	(cons 'composite ( unnest-composites (list (worker n))))
	(unnest-composites (worker n)))))

(defun print-0x (tic)
  "prints tic xenotation to 0X format"
  (let ((*print-pretty* t)
	(*print-pprint-dispatch* (copy-pprint-dispatch nil)))
    (set-pprint-dispatch 'null (lambda (stream obj) (format stream "()")))
    (if (eql (car tic) 'composite)
	(format t "~{~:S~}" (cdr tic))
	(format t "~:S" tic))))

(defun print-tx (tic)
  "prints tic xenotation to TX format "
  (let ((*print-pretty* t)
	(*print-pprint-dispatch* (copy-pprint-dispatch nil)))
    (set-pprint-dispatch 'null (lambda (stream obj) (format stream ":")))
    (if (eql (car tic) 'composite)
	(format t "~{~:S~}" (cdr tic))
	(format t "~:S" tic))))

(defun unnest-composites (tic)
  "given a nested list, e.g. (()), unnests an inner list that begins with the symbol COMPOSITE"
   (cond ((null tic) ()) 		;its nil, so it can't be unnested any more
	 ((eq (caar tic) 'composite) 	;the first element of the nested list is composite
	  (mapcar #'unnest-composites (cdar tic))) ;so return the result of unnesting the tail of nested
	 (t (list (unnest-composites (car tic)))))) ;otherwise, traverse downward without affecting anything
